package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class NaiveBayesUtils {

	public static void main(String[] args) throws IOException {
			
	}
	
	public static double computeApriori(ArrayList<InputImage> data, boolean isFace) {
		
		double numberOfObservations = 0.0;
		for(InputImage image : data) {
			if (image.getIsFace() == isFace) {
				numberOfObservations++;
			}
		}
		return numberOfObservations / data.size();		
	}
	
	/**	
	* <pre>
	* After using this method there would be created an array of (2*data) length
	* of aposterori probabilities which array can be described as follows(example):
	* 3.155989313276359E-48 = P(image1 | c = isFace)
	* 1.0639517354824473E-71 = P(image1 | c = isNotFace)
	* 1.897022254449526E-53  = P(image2 | c = isFace)
	* ...... </pre>*/	
	public static ArrayList<Double> computeAposterori(ArrayList<InputImage> data,
			ArrayList<ArrayList<Double>> probabilities, ArrayList<ArrayList <String>> fullDiscretizationLevels){
		
		Double probabilityFace = 1.0;
		Double probabilityNotFace = 1.0;
		ArrayList<Double> aposterori = new ArrayList<>();
		for (InputImage image : data) {
			ArrayList<String> hogDiscretization = image.getHogDiscretization();
			for (int i = 0; i < probabilities.size(); i++) {
				String hogValue = hogDiscretization.get(i);
				ArrayList<String> discretizationLevels = fullDiscretizationLevels.get(i);
				ArrayList<Double> attributeProbabilities = probabilities.get(i);
				for (int j = 0; j < discretizationLevels.size(); j++) {
					if (hogValue.equals(discretizationLevels.get(j))) {
						probabilityFace *= attributeProbabilities.get(j);
						probabilityNotFace *= attributeProbabilities.get(j + 10);
						break;
					}
				}
			}
			aposterori.add(probabilityFace);
			aposterori.add(probabilityNotFace);
			probabilityFace = 1.0;
			probabilityNotFace = 1.0;
		}
		
		return aposterori;		
	}
	

	/**	
	* <pre>
	* After using this method there would be created an array of arrays
	* numberOfAttributes x (2* numberOfDiscretizationLevels) which can
	* be described as follows(example):
	* 0.041379310344827586 = P(attribute1 = A1discretizationLevel1 | c = isFace)
	* 0.013793103448275862 = P(attribute1 = A1discretizationLevel1 | c = isNotFace)
	* 0.04827586206896552  = P(attribute1 = A1discretizationLevel2 | c = isFace)
	* ......
	*
	* The second subarray:
	* 0.15172413793103448  = P(attribute2 = A2discretizationLevel1 | c = isFace)
	* 0.1310344827586207   = P(attribute2 = A2discretizationLevel1 | c = isNotFace)
	* 0.12413793103448276  = P(attribute2 = A2discretizationLevel2 | c = isFace)
	* ...... </pre>*/	
	public static ArrayList<ArrayList<Double>> computeProbabilities(ArrayList<InputImage> data,
			ArrayList<ArrayList <String>> fullDiscretizationLevels){
				           
		ArrayList<ArrayList<Double>> fullProbabilities = new ArrayList<>();

		// filling new collection with collections and array with occurences
		// counters
		ArrayList<Double> al = null;
		for (int i = 0; i < data.get(0).getHogDiscretization().size(); i++) {
			al = new ArrayList<>();
			fullProbabilities.add(al);			
		}
		
		// initializing arrays, which would store number of faces (not-faces)
		// with exact attribute value
		ArrayList<Double> faceOccurences = new ArrayList<>();
		ArrayList<Double> notFaceOccurences = new ArrayList<>();
		for (int i = 0; i < fullDiscretizationLevels.get(0).size(); i++) {		
			faceOccurences.add(0.0);
			notFaceOccurences.add(0.0);
		}
		
		// counting faces and not-faces
		double numberOfFaces = 0.0;
		double numberOfNotFaces = 0.0;
		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).getIsFace() == true) {
				numberOfFaces += 1.0;
			} else {
				numberOfNotFaces += 1.0;
			}
		}
		
		for (int i = 0; i < data.get(0).getHogDiscretization().size(); i++) {
			
			ArrayList<String> attributeDiscretization = fullDiscretizationLevels.get(i);
			for (int j = 0; j < data.size(); j++) {

				String singleValue = data.get(j).getHogDiscretization().get(i);
				for(int k = 0; k < attributeDiscretization.size(); k++) {
					if(singleValue.equals(attributeDiscretization.get(k))) {
						if (data.get(j).getIsFace() == true) {
							faceOccurences.set(k, faceOccurences.get(k) + 1.0);
						} else {
							notFaceOccurences.set(k, notFaceOccurences.get(k) + 1.0);
						}						
						break;
					}
				}
				
			}
			
			// modyfing subarrays from fullProbabilities array; every subarray would
			// store 2 * (number of discretization levels) numbers
			ArrayList<Double> attributeProbabilities = fullProbabilities.get(i);
			for (Double d : faceOccurences) {
				attributeProbabilities.add(d / numberOfFaces);
			}
			for (Double d : notFaceOccurences) {
				attributeProbabilities.add(d / numberOfNotFaces);
			}
			
			for (int y = 0; y < faceOccurences.size(); y++) {		
				faceOccurences.set(y, 0.0);
				notFaceOccurences.set(y, 0.0);
			}
								
		}
		
		// handling the "zero-frequency" problem by replacing the 0 
		// probabilities with the lowest probability in the whole set
		ArrayList<Double> minimumProbabilities = new ArrayList<>();
		ArrayList<ArrayList<Double>> cloneFullProbabilities = new ArrayList<ArrayList<Double>>();
		for (ArrayList aList : fullProbabilities) {
			cloneFullProbabilities.add((ArrayList<Double>)aList.clone());
		}
		for (ArrayList<Double> alista : cloneFullProbabilities) {
			Collections.replaceAll(alista, 0.0, 0.99);
			minimumProbabilities.add(Collections.min(alista));
		}
		Double minimumProbability = Collections.min(minimumProbabilities);
		for (int q = 0; q < fullProbabilities.size(); q++) {
			ArrayList<Double> alist = fullProbabilities.get(q);
			for (int w = 0; w < alist.size(); w++) {
				if(alist.get(w) == 0.0) {
					alist.set(w, minimumProbability);
				}
			}
		}
		
		return fullProbabilities;		
	}
	
}
