package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiscretizationUtils {

	public static void main(String[] args) throws IOException {
        
	}
	
	/** this method may be used to test discretization - it will display number of records
	* assosiated with each discretization level; these numbers should be nearly equal to each
	* other
	*/
	public static void discretizationTest(ArrayList<InputImage> data) {
		
		ArrayList<String> discretizationLevels = new ArrayList<>();
		ArrayList<Integer> recordsNumber = new ArrayList<>();
		for (int m = 0; m < 10; m++) {
			recordsNumber.add(0);
		}
		for (int i = 0; i < data.get(0).getHogDiscretization().size(); i++) {
			for (int j = 0; j < data.size(); j++) {
				String level = data.get(j).getHogDiscretization().get(i);
				if (discretizationLevels.contains(level) == false) {
					discretizationLevels.add(level);
				}
				if (discretizationLevels.size() == 10) {
					break;
				}
			}
			boolean secondLoop = false;
			if (i == data.get(0).getHogDiscretization().size() && secondLoop == false) {
				i = 0;
				secondLoop = true;
			}
			for (int k = 0; k < data.size(); k++) {
				String level = data.get(k).getHogDiscretization().get(i);
				for (int n = 0; n < discretizationLevels.size(); n++) {
					if (level.equals(discretizationLevels.get(n))){
						int temp = recordsNumber.get(n) + 1;
						recordsNumber.set(n, temp);
					}
				}
			}
			for (int q = 0; q < recordsNumber.size(); q++) {
				System.out.println("Column " + (i+1) + ": " + recordsNumber.get(q));
			}
			discretizationLevels = new ArrayList<>();
			for (int m = 0; m < 10; m++) {
				recordsNumber.set(m, 0);
			}
		}
	}
	
	/** this method is used to set proper discretization levels in InputImage objects 
	*/
	public static void fillDiscretizationLevels(ArrayList<ArrayList<String>> fullDiscretizationLevels, ArrayList<InputImage> data) {

		ArrayList<Double> hogs = null;
		for (InputImage image : data) {
			hogs = image.getHog();
			ArrayList<String> hogDiscretization = new ArrayList<>();
			// i - set of levels for each attribute
			// j - level
			for (int i = 0; i < fullDiscretizationLevels.size(); i++) {
				ArrayList<String> discretizationLevels = fullDiscretizationLevels.get(i);
				Double singleHog = hogs.get(i);
				for (int j = 0; j < discretizationLevels.size(); j++) {
					String strLevel = discretizationLevels.get(j);
					Double dblLevel = Double.valueOf(strLevel.substring(1, strLevel.indexOf(",")));
					if (singleHog < dblLevel) {						
						if (j == 0) {
							hogDiscretization.add(discretizationLevels.get(0));	
							break;
						}
						hogDiscretization.add(discretizationLevels.get(j-1));				
						break;
					}
					if (j ==  discretizationLevels.size() - 1) {
						hogDiscretization.add(strLevel);
					}
				}
			}
			image.setHogDiscretization(hogDiscretization);
		}
	}
	
	/** this method is used to provide domains of each attribute for attributeDiscretization(...) method
	* and for invoking attributeDiscretization(...) many times
	*/
	public static ArrayList<ArrayList <String>> fullDiscretization (ArrayList<InputImage> data){
		
		ArrayList<ArrayList <String>> fullDiscretizationLevels = new ArrayList<>();
		ArrayList <String> discretizationLevels = null;
		ArrayList <Double> singleAttributeDomain = null;
		// iteration goes through attributes (columns), so in the loop end condition
		// is size of array of HOG values of any (e.g. first) image
		for (int i = 0; i < data.get(0).getHog().size(); i++) {
			singleAttributeDomain = new ArrayList<>();
			discretizationLevels = new ArrayList<>();
			for (InputImage image: data) {
				singleAttributeDomain.add(image.getHog().get(i));
	        }
			discretizationLevels = attributeDiscretization(singleAttributeDomain);
			fullDiscretizationLevels.add(discretizationLevels);
		}
		return fullDiscretizationLevels;		
	}
	
	/** <pre> 
	  * this method is used to make discretization of column (attribute);
	  * it is needed to provide attribute domain (all values which occur
	  * in that column) for this method; the output is an array of 10 strings
	  * which represent value levels. Example of output:
	  * [0.0000501084059425047000000, 0.0005500712870540780000000)
	  * [0.0005500712870540780000000, 0.0010103739697666000000000)
      * [0.0010103739697666000000000, 0.0014429277882000100000000)
      * ............. </pre>
	  */
	public static ArrayList<String> attributeDiscretization(ArrayList<Double> attributeValues) {
		
		ArrayList<String> discretizationLevels = new ArrayList<String>();
		Collections.sort(attributeValues);
		int threshold = (int)(attributeValues.size() / 10);		
		int i = 0;
		StringBuilder sb = new StringBuilder();
		while(i < 9*threshold) {
			sb.append("[");
			sb.append(String.format("%.25f", attributeValues.get(i)));		
			sb.append(", ");
			i += threshold;
			sb.append(String.format("%.25f", attributeValues.get(i)));
			sb.append(")");
			discretizationLevels.add(sb.toString());
			sb = new StringBuilder();
		}
		sb.append("[");
		sb.append(String.format("%.25f", attributeValues.get(i)));
		sb.append(", ");
		sb.append(String.format("%.25f", attributeValues.get(attributeValues.size()-1) + 1.0));
		sb.append(")");
		discretizationLevels.add(sb.toString());
		return discretizationLevels;
	}
	
	
	/** <pre> 
	 * this method is used to invoke parseData(...) many times,
	 * i.e. to parse data organized like this:
	 * "101.jpg"	"1"	0.00911923124245046	0.000816583209076381 ...
	 * to chosen object - InputImage designed for storing data
	 * </pre> 
	*/
	public static ArrayList<InputImage> bigParseData(List<String> lines, boolean isClassKnown) {
		
		ArrayList<InputImage> imagesList = new ArrayList<>();
		lines.remove(0);
		for (String line : lines) {
			imagesList.add(parseData(line, isClassKnown));
		}
		
		return imagesList;
	}
	
	/** <pre>
	 *  this method is used to support reading data from file,
	 *  i.e. parsing data organized like this:
	 *  "101.jpg"	"1"	0.00911923124245046	0.000816583209076381 ...
	 *  to chosen object - InputImage designed for storing data
	 */
	public static InputImage parseData(String line, boolean isClassKnown) {
		
		StringBuilder sb = new StringBuilder();
		InputImage image = new InputImage();
		
		int i = 1;
		while(line.charAt(i) != ((char)34)) {
			sb.append(line.charAt(i));
			i++;
		}
		image.setFileName(sb.toString());
		
		if (isClassKnown == true) {		
			
			if(line.charAt(line.lastIndexOf(((char)34)) - 1) == '0') {
				image.setIsFace(false);
			} else {
				image.setIsFace(true);
			}
		} else {
			image.setIsFace(null);
		}
		
		sb = new StringBuilder();
		int j = line.lastIndexOf(((char)34)) + 2;
		ArrayList<Double> hog = new ArrayList<>();
		for (; j < line.length(); j++) {
			
			if(line.charAt(j) != '\t') {
				sb.append(line.charAt(j));
			} else {
				hog.add(Double.valueOf(sb.toString()));
				sb = new StringBuilder();
			}
		}
		hog.add(Double.valueOf(sb.toString()));
		image.setHog(hog);
		image.setHogDiscretization(null);
		
		return image;		
	}
	
}
