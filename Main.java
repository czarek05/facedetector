package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			FXMLLoader loader = new FXMLLoader(Main.class.getResource("GUI.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.sizeToScene();
			primaryStage.show();
	        primaryStage.setMinWidth(primaryStage.getWidth());
	        primaryStage.setMinHeight(primaryStage.getHeight());
	        Controller controller = loader.getController();
	        controller.setMain(this);
	        String description = "This program takes from user paths to files "
	        		+ "with computed HOGs and path to a place where the output should "
	        		+ "be saved. After pressing the button, program performs a discretization "
	        		+ "for naive bayes classifer, build naive bayes classification model and "
	        		+ "performs prediction. Then application prints the output below the "
	        		+ "button and prints the output to the specified file.";
	        Alert alert = new Alert(AlertType.CONFIRMATION, description, ButtonType.OK);
	        alert.setTitle("APPLICATION DESCRIPTION");
	        alert.setHeaderText("");
	        alert.showAndWait();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
