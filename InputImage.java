package application;

import java.util.ArrayList;

public class InputImage {

	private String fileName;
	private Boolean isFace;
	private ArrayList<Double> hog;
	private ArrayList<String> hogDiscretization;
	private Boolean isFacePrediction;
	
	public InputImage() {
		
	}
	
	public InputImage(String fileName, Boolean isFace, ArrayList<Double> hog, ArrayList<String> hogDiscretization) {
		
		this.fileName = fileName;
		this.isFace = isFace;
		this.hog = hog;
		this.hogDiscretization = hogDiscretization;
		
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean getIsFace() {
		return isFace;
	}

	public void setIsFace(Boolean isFace) {
		this.isFace = isFace;
	}

	public ArrayList<Double> getHog() {
		return hog;
	}

	public void setHog(ArrayList<Double> hog) {
		this.hog = hog;
	}

	public ArrayList<String> getHogDiscretization() {
		return hogDiscretization;
	}

	public void setHogDiscretization(ArrayList<String> hogDiscretization) {
		this.hogDiscretization = hogDiscretization;
	}

	public Boolean getIsFacePrediction() {
		return isFacePrediction;
	}

	public void setIsFacePrediction(Boolean isFacePrediction) {
		this.isFacePrediction = isFacePrediction;
	}
	
}
