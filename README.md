# FaceDetector

## Project Description 
The aim of the project was to implement an application for face detection purposes. The application performs a discretization for naive Bayes classificator, trains the naive Bayes model and performs a prediction. Discretization and naive Bayes model were implemented from the very beginning. The application performs a classification of input images, i.e. decide whether there is a face on the image or there are no faces on the image. There were made some necessary assumptions:

- resolution of every input image is equal,
- areas which are covered by the faces are similar (i.e. without situations, where one face cover 90% of the image and another face cover 10% of the image),
- faces are turned more or less frontal,
- skin colour of the faces is similar to each other,
- there are no glasses etc. objects on the faces and faces are not hidden by any other obstacles,
- lighting of images is quite similar,
- there are no strange expressions on the faces, the expressions are "normal".

These assumptions were mostly covered (there were some images of people with darker skin, one or two people wear earrings or peaked caps) by chosen data - 200 images from a face database. All images were resized to resolution 80x100 and cropped that face covered about 100% of image. The other part of the data was a set of 200 random images for the Internet (also resized to resolution 80x100). So, together 400 images were used.

Feature extraction was made using histogram of oriented gradients feature descriptor from OpenImageR library in R language. It provided the data as a file with attributes (columns) and values (rows) to work with. Also, in R the data was splitted on training set (about 70% of primary set)and testing set (about 30% of primary set).

After computing the HOG, it was possible to use the application. The application input were files with computed HOG and the application output is file with prediction results, values of numerator of Baye's equation for being face and not being face, confusion matrices. 

The discretization was performed as follows: for every attribute (column) values were sorted to obtain ascending order. Then, the values of  lowest HOG and (let me say) "HOG nr.:" "training set / 10" were taken and a range (level) like below was created: 

[0.0000164472317334528000000, 0.0024581892963680100000000)

then the values of HOG nr "training set / 10" and "(training set / 10) + (training set / 10)" were taken to obtain a range(level) like below:

[0.0024581892963680100000000, 0.0035650290931808900000000)

and so on, to obtain 10 such levels for every attribute (column). According to theory, it should be a better discretization way (creating a X (10), equaly saturated of records, levels), than performing a discretization with threshold: (MAX - MIN) / (number of levels).

After the discretization, building of naive Bayes model was made, i. a. computing aposterori probabilities. The application took every value for every attribute and adequate discretization levels(ranges), and check how many records have this value: 

[0.0024581892963680100000000, 0.0035650290931808900000000)

for first attribute and how many from them are faces and how many from them are not faces. This kind of calculations was performed for every level for every attribute, so the matrix of constituent probabilities had size of (number_of_attributes)x(number_of_levels* 2) (for being face and not being face). 

## Usage

### R part

Organise your data, so that 200 faces and 200 not-faces are under same localisation, which match localisation from `Face_Detection.R` file. Name faces: 101.jpg, 102.jpg, ..., 300.jpg and not-faces: 301.jpg, 302.jpg, ..., 500jpg. Run script, which would create `sampleTrainData.txt` and `sampleTestData.txt` files. Alternatively omit R part and just use attached files.

### Java part

Run application, provide localisations of `sampleTrainData.txt`,`sampleTestData.txt` and where you wish to get output (like: `F:\output.txt`). Click on the button "COMPUTE". 
