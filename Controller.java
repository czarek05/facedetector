package application;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller {
	
	@FXML
	private TextField pathTrain;
	@FXML
	private TextField pathTest;
	@FXML
	private TextField pathOutput;
	@FXML
	private TextArea logs;
	@FXML
	private Button launchButton;
	
	private Main main;
	
	public void setMain(Main main) {
		this.main = main;
	}
	
	public void handleButton() throws IOException {
		List<String> linesTrain = Files.readAllLines(Paths.get(pathTrain.getText()));
		List<String> linesTest = Files.readAllLines(Paths.get(pathTest.getText()));
		Locale.setDefault(Locale.US);
		ArrayList<InputImage> trainData = DiscretizationUtils.bigParseData(linesTrain, true);
		ArrayList<InputImage> testData = DiscretizationUtils.bigParseData(linesTest, true);
		ArrayList<ArrayList <String>> fullDiscretizationLevels = DiscretizationUtils.fullDiscretization(trainData);
		DiscretizationUtils.fillDiscretizationLevels(fullDiscretizationLevels, trainData);
		DiscretizationUtils.fillDiscretizationLevels(fullDiscretizationLevels, testData);
		double aprioriFace = NaiveBayesUtils.computeApriori(trainData, true);
		double aprioriNotFace = NaiveBayesUtils.computeApriori(trainData, false);
		ArrayList<ArrayList<Double>> fullProbabilities = NaiveBayesUtils.computeProbabilities(trainData, fullDiscretizationLevels);
		ArrayList<Double> aposteroriTrain = NaiveBayesUtils.computeAposterori(trainData, fullProbabilities, fullDiscretizationLevels);
		ArrayList<Double> aposteroriTest = NaiveBayesUtils.computeAposterori(testData, fullProbabilities, fullDiscretizationLevels);
	
		// computing numerators for Bayes equation, checking which numerator (for face
		// and not face) is bigger and setting prediction results for all images;
		// numerator i. e.:
		// P(image | Ci) * P(Ci)
		// these numerators will be given as the output to compare how "certain" in
		// classification this application was
		StringBuilder content = new StringBuilder();
		DecimalFormat df = new DecimalFormat( "0.00000000000E0" );
		content.append("PREDICTION ON TRAIN DATA\n\n\n");
		content.append("fileName\t\tisFace\t\t\tisFacePrediction\t\t\t\tnumeratorIsFace\t\t\t\t\t\tnumeratorIsNotFace\n");
		int i = 0;
		for (InputImage image : trainData) {
			if (aposteroriTrain.get(i) * aprioriFace >= aposteroriTrain.get(i+1) * aprioriNotFace) {				
				image.setIsFacePrediction(true);
			} else {
				image.setIsFacePrediction(false);
			}
			content.append(image.getFileName());
			content.append("\t\t\t");
			content.append(image.getIsFace());
			content.append("\t\t\t");
			content.append(image.getIsFacePrediction());
			content.append("\t\t\t\t\t\t");
			content.append(df.format(aposteroriTrain.get(i) * aprioriFace));
			content.append("\t\t\t\t\t");
			content.append(df.format(aposteroriTrain.get(i+1) * aprioriNotFace));
			content.append("\n");
			i += 2;
		}
		content.append("\n\n\n\n\n\n\n\n\n\n\n");
		content.append("PREDICTION ON TEST DATA\n\n\n");
		content.append("fileName\t\tisFace\t\t\tisFacePrediction\t\t\t\tnumeratorIsFace\t\t\t\t\t\tnumeratorIsNotFace\n");
		i = 0;
		for (InputImage image : testData) {
			if (aposteroriTest.get(i) * aprioriFace >= aposteroriTest.get(i+1) * aprioriNotFace) {
				image.setIsFacePrediction(true);
			} else {
				image.setIsFacePrediction(false);
			}
			content.append(image.getFileName());
			content.append("\t\t\t");
			content.append(image.getIsFace());
			content.append("\t\t\t");
			content.append(image.getIsFacePrediction());
			content.append("\t\t\t\t\t\t");
			content.append(df.format(aposteroriTrain.get(i) * aprioriFace));
			content.append("\t\t\t\t\t");
			content.append(df.format(aposteroriTrain.get(i+1) * aprioriNotFace));
			content.append("\n");
			i += 2;
		}
		
		// computing confusion matrices		
		int correctFace = 0;
		int correctNotFace = 0;
		int incorrectFace = 0;
		int incorrectNotFace = 0;
		for (InputImage temp : trainData) {
			if (temp.getIsFace() == temp.getIsFacePrediction()) {
				if(temp.getIsFace() == true) {
					correctFace++;
				} else {
					correctNotFace++;
				}
			} else {
				if(temp.getIsFace() == true) {
					incorrectNotFace++;
				} else {
					incorrectFace++;
				}
			}
		}
		content.append("\n\n\n\n\n\n\n\n\n\n\n");
		content.append("CONFUSION MATRCES");
		content.append("\n\n\n");
		content.append("SCHEMA:\n");
		content.append("\t\t\t\t\t\tClass predicted\n");
		content.append("Real class\t\t\t\t\tFace\t\tNotFace\n");
		content.append("Face\t\t\t\t\t\tTrueFace\tFalseNotFace\nNotFace");
		content.append("\t\t\t\t\t\tFalseFace\tTrueNotFace");
		content.append("\n\n\nTRAINING DATA:\n");
		content.append("\t\t\t\t\t\tClass predicted\n");
		content.append("Real class\t\t\t\t\tFace\t\tNotFace\n");
		content.append("Face\t\t\t\t\t\t");
		content.append(correctFace);
		content.append("\t\t");
		content.append(incorrectNotFace);
		content.append("\nNotFace\t\t\t\t\t\t");
		content.append(incorrectFace);
		content.append("\t\t");
		content.append(correctNotFace);
		
		correctFace = 0;
		correctNotFace = 0;
		incorrectFace = 0;
		incorrectNotFace = 0;
		for (InputImage temp : testData) {
			if (temp.getIsFace() == temp.getIsFacePrediction()) {
				if(temp.getIsFace() == true) {
					correctFace++;
				} else {
					correctNotFace++;
				}
			} else {
				if(temp.getIsFace() == true) {
					incorrectNotFace++;
				} else {
					incorrectFace++;
				}
			}
		}
		content.append("\n\n\nTESTING DATA:\n");
		content.append("\t\t\t\t\t\tClass predicted\n");
		content.append("Real class\t\t\t\t\tFace\t\tNotFace\n");
		content.append("Face\t\t\t\t\t\t");
		content.append(correctFace);
		content.append("\t\t");
		content.append(incorrectNotFace);
		content.append("\nNotFace\t\t\t\t\t\t");
		content.append(incorrectFace);
		content.append("\t\t");
		content.append(correctNotFace);
		logs.setText(content.toString());
		// byte[] b = content.toString().getBytes();
		// byte[] b = content.toString().getBytes(Charset.forName("UTF-8"));
		String c = content.toString();
		c = c.replaceAll("\n","\r\n");
		byte[] b = c.toString().getBytes(StandardCharsets.UTF_8);
		Files.write(Paths.get(pathOutput.getText()), b);

	}
	
	

}
